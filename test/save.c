#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "structs.h"
#include "save.h"
#include "game/entities.h"

void
saveGame(Fortress *fortress)
{
    FILE *f = fopen("save.txt", "w");
    Entity *e = fortress->entityList;
    if (f == NULL)
    {
        printf("ERREUR !");
        exit(1);
    }
    else
    {
        fprintf(f, "%d\n", fortress->map.num);
        fprintf(f, "%d\n", fortress->map.seed);

        while (e != NULL){
            fprintf(f, "%d %d %d %d %d\n", e->x, e->y, e->type, e->team, e->life);

            e = e->next;
        }
    }
    fclose(f);
}

void
loadGame(Fortress *fortress)
{
    FILE *f = fopen("save.txt", "r");
    int i, tmp[5];
    if (f == NULL)
    {
        printf("ERREUR !");
        exit(1);
    }
    fscanf(f, "%d", &fortress->map.num);
    fscanf(f, "%d", &fortress->map.seed);
    
    while( !feof(f))
    {
        for(i = 0; i < 5; i++){
            fscanf(f, "%d", &tmp[i]);
        }
        createEntity(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]);
        fprintf(stderr, "new entity : x : %d y : %d type : %d team : %d life : %d\n", tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]);
    }

    fclose(f);
}
