/**
 * \file save.h
 *
 * Header file to save the game.
 */

#ifndef SAVE_H
#define SAVE_H
#include <SDL2/SDL.h>
#include "structs.h"

/**
 * Function to save the game in a txt file.
 */
void
saveGame(Fortress *fortress);

/**
 * Function to load the game using a txt file.
 */
void
loadGame(Fortress *fortress);

#endif /* SAVE_H */
