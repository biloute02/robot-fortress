# TODO
## IMPORTANT

## INPUT
- Lag on detecting SDL\_BUTTON\_LEFT espcially when the processor is running at low frequency.

## FORTRESS
- initFortress too long and duplicate code.
- Must test the return values of sendNet and recvNet.

## ENTITIES
- Entities which have a team must have a version of the image for each color.
- Texture of entity depends on his life value.

## ACTIONS
- useActionInfo save the x y coordinates of mouse instead of the map.
- gather/separate doAction and useActionInfo.

## GRAPHICS
- Image for blue and red mine is the same.
- dead unit texture?

## FOR THE FUTUR
- Add bridges.
- Add more teams.
