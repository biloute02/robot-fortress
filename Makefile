CC = gcc
CFLAGS = -g -std=c99 -pedantic -Wall -Wextra
LDLIBS = -lSDL2 -lSDL2_image -lSDL2_ttf

VPATH = src src/system src/game
DEPS = defs.h struct.h
OBJ = main.o init.o input.o draw.o util.o text.o menu.o widgets.o game.o fortress.o texture.o map.o entities.o actionMenu.o actionLogic.o borders.o network.o

all: main 

main: $(OBJ)

.PHONY: clean

clean:
	$(RM) *.o main
