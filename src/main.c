/**
 * \file main.c
 *
 * Main program.
 */

#include <SDL2/SDL.h>
#include "main.h"
#include "structs.h"
#include "defs.h"
#include "system/init.h"
#include "system/input.h"
#include "system/draw.h"
#include "system/util.h"
#include "system/text.h"
#include "system/texture.h"
#include "system/network.h"
#include "game/menu.h"
#include "game/map.h"
#include "game/game.h"
#include "game/entities.h"

App app = {0};

int main(int argc, char *argv[])
{
    if (argc > 1) {
        if (strcmp("-s", argv[1]) == 0 && argc > 2) {
            app.net.hostname = argv[2];
        } else {
            printf("Usage: %s [OPTION]\n", argv[0]);
            printf("-s HOSTNAME     se connecte au server HOSTNAME\n");
            exit(EXIT_FAILURE);
        }
    }

    initSDL();
    initFonts();
    initMenu();
    atexit(cleanup);


    while (1) {
        prepareScene();

        doInput();

        app.delegate.logic();
        app.delegate.draw();

        presentScene();

        // Traiter une interruption du déroulement de l'application :
        // (changement d'écran, sauvegarde, connexion...);
        if (app.delegate.interrupt != NULL) {
            app.delegate.interrupt();
            app.delegate.interrupt = NULL;
        }

        SDL_Delay(16);
    }

    return 0;
}
