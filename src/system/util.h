/**
 * \file util.h
 *
 * Header file for files functions.
 */

#ifndef UTIL_H
#define UTIL_H

/**
 * Return the length of the longest line in a file.
 *
 * \param f A valid pointer to file.
 */
int longestLine(FILE *f);

/**
 * Return the number of lines in a file.
 *
 * \param f A valid pointer to file.
 */
int numberOfLines(FILE *file);

/**
 * Function to read each line of a file into an array of strings
 * terminating by NULL.
 * The array is allocated and must be freed by the user.
 */
char **readFile(char *filename);

/**
 * Return the map in a 2d array
 */
int **getMapArray(char*);

#endif /* UTIL_H */
