#include <SDL2/SDL.h>
#include "input.h"
#include "../main.h"

static void
doMouseButtonUp(SDL_MouseButtonEvent *event)
{
    if (event->button < MAX_MOUSE_KEYS) {
        app.mouse.button[event->button] = 0;
    }
}

static void
doMouseButtonDown(SDL_MouseButtonEvent *event)
{
    if (event->button < MAX_MOUSE_KEYS) {
        app.mouse.button[event->button] = 1;
    }
}

static void
doKeyUp(SDL_KeyboardEvent *event)
{
    if (event->repeat == 0 && event->keysym.sym < MAX_KEYBOARD_KEYS) {
        app.keyboard[event->keysym.sym] = 0;
    }
}

static void
doKeyDown(SDL_KeyboardEvent *event)
{
    if (event->repeat == 0 && event->keysym.sym < MAX_KEYBOARD_KEYS) {
        app.keyboard[event->keysym.sym] = 1;
    }
}

void
doInput(void)
{
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                exit(0);
                break;
            case SDL_KEYDOWN:
                doKeyDown(&event.key);
                break;
            case SDL_KEYUP:
                doKeyUp(&event.key);
                break;
            case SDL_MOUSEBUTTONDOWN:
                doMouseButtonDown(&event.button);
                break;
            case SDL_MOUSEBUTTONUP:
                doMouseButtonUp(&event.button);
                break;
            default:
                break;
        }
    }

    SDL_GetMouseState(&app.mouse.x, &app.mouse.y);
}
