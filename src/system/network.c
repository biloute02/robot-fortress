#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "network.h"
#include "../main.h"
#include "../defs.h"

/**
 * Terminate dead processes.
 */
static void sigchld_handler(int s);

/**
 * Get sockaddr, IPv4 or IPv6.
 */
static void *get_in_addr(struct sockaddr *sa);

void
initServer(void)
{
    int sockfd; // Listen on sockfd.
    struct addrinfo hints, *servinfo, *p;
    struct sigaction sa;
    int yes = 1;
    int rv;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET; // Use IPv4 address.
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // Use my IP.

    if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        exit(EXIT_FAILURE);
    }

    // Loop through all the results and bind to the first we can.
    for (p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
            perror("setsockopt");
            exit(EXIT_FAILURE);
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("server: bind");
            continue;
        }

        break;
    }
    freeaddrinfo(servinfo); // All done with this structure.

    if (p == NULL) {
        fprintf(stderr, "server: failed to bind\n");
        exit(EXIT_FAILURE);
    }

    if (listen(sockfd, BACKLOG) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    sa.sa_handler = sigchld_handler; // Reap all dead processes.
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }

    app.net.serverSockfd = sockfd; // The most important line.
}

int
acceptClient(void)
{
    struct sockaddr_storage their_addr; // Connector's address information.
    socklen_t sin_size;
    char s[INET6_ADDRSTRLEN]; // Address of the connected client.


    // Close any previous connexion.
    if (app.net.clientSockfd != 0) {
        close(app.net.clientSockfd);
    }
    fprintf(stderr, "clientSock: %d\n", app.net.clientSockfd);
    fprintf(stderr, "serverSock: %d\n", app.net.serverSockfd);

    printf("server: waiting for connections...\n");

    sin_size = sizeof(their_addr);
    app.net.clientSockfd = accept(app.net.serverSockfd,
            (struct sockaddr *) &their_addr, &sin_size);
    if (app.net.clientSockfd == -1) {
        perror("accept");
        return -1;
    }

    inet_ntop(their_addr.ss_family,
        get_in_addr((struct sockaddr *) &their_addr),
        s, sizeof(s));
    printf("server: got connection from %s\n", s);

    return 0;
}

int
initClient(void)
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];

    // Check if hostname exists.
    if (app.net.hostname == NULL) {
        fprintf(stderr, "client: hostname not defined\n");
        return -1;
    }

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo(app.net.hostname, PORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return -1;
    }

    // Loop through all the results and connect to the first we can.
    for (p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                        p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }
        
        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }

        break;
    }
    freeaddrinfo(servinfo);
    
    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return -1;
    }

    inet_ntop(p->ai_family,
            get_in_addr((struct sockaddr *) p->ai_addr), s, sizeof(s));
    printf("client: connecting to %s\n", s);

    app.net.clientSockfd = sockfd;
    return 0;
}

int 
sendNet(const void *buf, size_t len)
{
    return send(app.net.clientSockfd, buf, len, 0);
}

int
recvNet(void *buf, size_t len)
{
    return recv(app.net.clientSockfd, buf, len, 0);
}

char *
getIP(void)
{
    char *ip;
    char hostbuffer[256];
    struct hostent *host_entry;

    // Get the hostname of the computer
    gethostname(hostbuffer, sizeof(hostbuffer)); 

    // Get all the informations from the host
    host_entry = gethostbyname(hostbuffer); 

    // Get the IP from the host_entry (in the string data type)
    ip = inet_ntoa(*((struct in_addr*) host_entry->h_addr_list[0])); 

    return ip;
}

static void
sigchld_handler(int s)
{
    (void)(s);
    // Waitpid() might overwrite errno, so we save and restore it.
    int saved_errno = errno;
    while (waitpid(-1, NULL, WNOHANG) > 0);
    errno = saved_errno;
}

/**
 * Get sockaddr, IPv4 or IPv6.
 */
static void *
get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*) sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*) sa)->sin6_addr);
}

