#include <SDL2/SDL.h>
#include "draw.h"
#include "../main.h"

void
prepareScene(void)
{
    SDL_SetRenderDrawColor(app.renderer, 96, 128, 255, 255);
    SDL_RenderClear(app.renderer);
}

void
presentScene(void)
{
    SDL_RenderPresent(app.renderer);
}

void
blit(SDL_Texture *texture, int x, int y, int scale)
{
    SDL_Rect dest;

    dest.x = x;
    dest.y = y;
    SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
    dest.w *= scale;
    dest.h *= scale;
    SDL_RenderCopy(app.renderer, texture, NULL, &dest);
}

void
blitRect(SDL_Rect *rect, SDL_Color color)
{
    SDL_SetRenderDrawColor(app.renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(app.renderer, rect);
}
