#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <unistd.h>
#include "texture.h"
#include "../main.h"
#include "../defs.h"

SDL_Texture *
loadTexture(const char *filename)
{
    SDL_Texture *texture;
    SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION,
            SDL_LOG_PRIORITY_INFO, "Loading %s", filename);
    texture = IMG_LoadTexture(app.renderer, filename);
    if (texture == NULL) {
        fprintf(stderr,"IMG_LoadTexture: %s\n", filename);
        exit(EXIT_FAILURE);
    }
    return texture;
}

SDL_Texture **
loadAllTextures(const char *dir, int num, int color)
{
    char filename[MAX_FILENAME_LENGTH];
    SDL_Texture **tab;

    if (color) {
        tab = (SDL_Texture **) calloc(num * NB_COLORS, sizeof(SDL_Texture *));
        for (int i = 0; i < num; i++) {
            sprintf(filename, "%s/blue/%d.png", dir, i + 1);
            tab[BLUE * num + i] = loadTexture(filename);
            sprintf(filename, "%s/red/%d.png", dir, i + 1);
            tab[RED * num + i] = loadTexture(filename);
        }
    // No colors
    } else {
        tab = (SDL_Texture **) calloc(num, sizeof(SDL_Texture *));
        for (int i = 0; i < num; i++) {
            sprintf(filename, "%s/%d.png", dir, i + 1);
            tab[i] = loadTexture(filename);
        }
    }

    return tab;
}

SDL_Texture *
toTexture(SDL_Surface *surface, int destroySurface)
{
    SDL_Texture *texture;
    texture = SDL_CreateTextureFromSurface(app.renderer, surface);
    if (destroySurface) {
        SDL_FreeSurface(surface);
    }
    return texture;
}
