/**
 * \file texture.h
 * Functions for managing textures and surfaces.
 */

/**
 * Load the image specified by filename into the renderer
 * and return its texture.
 */
SDL_Texture *loadTexture(const char *filename);

/**
 * Load [number] textures from PNG images in the directory [dir].
 * The images must be number from 1 to [number] like 'dir/[1..num].png'.
 * The array of textures are allocated and must be freed by the user.
 * If [color] is different than 0, then use the colored versions
 * of the texture placed in subfolders like 'dir/red/[1..num].png'.
 *
 * \param dir The path of the directory.
 * \param num The number of images inside the directory.
 * \param color true if we use colored team textures.
 * \return An array of textures.
 */
SDL_Texture **loadAllTextures(const char *dir, int num, int color);

/**
 * Transform a surface into a texture and return it.
 * Free the surface if destroySurface is true.
 */
SDL_Texture *toTexture(SDL_Surface *surface, int destroySurface);
