/**
 * \file draw.h
 *
 * Header file for drawing functions.
 */

#ifndef DRAW_H
#define DRAW_H


/**
 * Reset the renderer.
 */
void prepareScene(void);

/**
 * Update the screen.
 */
void presentScene(void);

/**
 * Draw [texture] on screen at [x] and [y] coordinates with the zoom [scale].
 */
void blit(SDL_Texture *texture, int x, int y, int scale);

/**
 * Draw [rect] with [color] on renderer.
 */
void blitRect(SDL_Rect *rect, SDL_Color color);

#endif /* DRAW_H */
