#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "init.h"
#include "../main.h"
#include "../defs.h"

void
initSDL(void)
{
    int windows_flags, renderer_flags;
    windows_flags = 0;
    renderer_flags = SDL_RENDERER_ACCELERATED;
    int img_flags = IMG_INIT_PNG | IMG_INIT_JPG;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL n'a pas pu être initialisée : %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    app.window = SDL_CreateWindow("Robot Fortress",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH, SCREEN_HEIGHT, windows_flags);
    if (!app.window) {
        fprintf(stderr, "La fenêtre n'a pas pu être créée : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    app.renderer = SDL_CreateRenderer(app.window, -1, renderer_flags);
    if (!app.renderer) {
        fprintf(stderr, "Le renderer n'a pas pu être créé : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

	if (IMG_Init(img_flags) != img_flags) {
        fprintf(stderr, "SDL IMG n'a pas pu être initialisée\n");
        exit(EXIT_FAILURE);
    }

    if (TTF_Init() < 0) {
        fprintf(stderr, "SDL TTF n'a pas pu être initialisée : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
}

void
cleanup(void)
{
    SDL_DestroyRenderer(app.renderer);
    SDL_DestroyWindow(app.window);
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
}
