/**
 * \file text.h
 *
 * Header file for text rendering.
 */

#ifndef TEXT_H
#define TEXT_H

/**
 * Initialize the program's font.
 */
void initFonts(void);

/**
 * Create the texture corresponding to text using the global font.
 */
SDL_Texture *getTextTexture(char *text);


#endif /* TEXT_H */
