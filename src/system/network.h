/**
 * \file network.h
 *
 * Header file for ip functions
 */

#ifndef NETWORK_H
#define NETWORK_H
#include "../defs.h"

/**
 * Create a new server listening TCP socket.
 */
void initServer(void);

/**
 * Server will accept the first incoming connection
 * and created a new TCP connection socket with a client.
 *
 * \return 0 if the connexion is successful, else -1.
 */
int acceptClient(void);

/**
 * Create a client TCP socket and initiate the connexion with the server.
 *
 * \return 0 if the connexion is successful, else -1.
 */
int initClient(void);

/**
 * Send data to the connection socket.
 */
int sendNet(const void *buf, size_t len);

/**
 * Receive data from the connection socket.
 */
int recvNet(void *buf, size_t len);

/**
 * Get the computer's IP address.
 */
char *getIP(void);

#endif /* NETWORK_H */
