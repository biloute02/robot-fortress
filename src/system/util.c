#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "util.h"
#include "../main.h"
#include "../defs.h"

int
longestLine(FILE *f)
{
    int line_size = 0;
	int max_size = 0;
    int c;
	fseek(f, 0, SEEK_SET);
	while ((c = fgetc(f)) != EOF) {
        line_size++;
        if (c == '\n') {
            if (line_size > max_size) max_size = line_size;
            line_size = 0;
        }
	}
	return max_size;
}

int
numberOfLines(FILE *f)
{
    int c;
	int nline = 0;
	fseek(f, 0, SEEK_SET);
	while ((c = fgetc(f)) != EOF) {
        if (c == '\n') nline++;
    }
	return nline;
}

char **
readFile(char *filename)
{	char **tab; FILE *f = fopen(filename, "r");
	if (f == NULL) {
		fprintf(stderr, "readFile: cannot open file %s\n", filename);
		exit(EXIT_FAILURE);
	}

	int flength = numberOfLines(f);
	tab = (char **) malloc(sizeof(char *) * (flength + 1));

	int llength = longestLine(f);
	char line[llength + 1];

	fseek(f, 0, SEEK_SET);
	for (int i = 0; i < flength; i++) {
		fgets(line, llength + 1, f);
		int line_size = strlen(line);
		tab[i] = (char *) malloc(sizeof(char) * (line_size + 1));
		strcpy(tab[i], line);
        // Suppress the new line caracter.
		tab[i][line_size - 1] = '\0';
	}
    fclose(f);

    // Tab is NULL terminated.
	tab[flength] = NULL;
    return tab;
}


int**
getMapArray(char* filename){
    int data, row, col, tmp;
    char car;
    FILE* f = fopen(filename,"r");
    int **array = (int**) malloc(sizeof(int *) * MAP_HEIGHT);
    for(int i = 0; i < MAP_HEIGHT; i++) array[i] = (int *) malloc(sizeof(int) * MAP_WIDTH);
    if(f) {
	    row=col=0;
	    while(EOF!=(tmp=fscanf(f,"%d%c", &data, &car))){
		printf("%d %d : %d\n", row, col, data);
        array[row][col++] = data;
		    if(car == '\n'){
                row++; // If \n -> next row
                col = 0;
            }
	    }
    }
    else fprintf(stderr, "ERROR !\n");
    fclose(f);
    return array;
}
