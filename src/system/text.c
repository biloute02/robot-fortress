#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "text.h"
#include "../defs.h"
#include "../main.h"
#include "texture.h"

static TTF_Font *font;
static SDL_Color white = {255, 255, 255, 255};

void
initFonts(void)
{
    font = TTF_OpenFont("fonts/NotoSans-Medium.ttf", FONT_SIZE);
}

SDL_Texture *
getTextTexture(char *text)
{
    SDL_Surface *surface;
    surface = TTF_RenderUTF8_Blended(font, text, white);
    return toTexture(surface, 1);
}
