/**
 * \file init.h
 *
 * Header file for initialising SDL. 
 */

#ifndef INIT_H
#define INIT_H

/**
 * Initialise the SDL window and renderer.
 */
void initSDL(void);

/**
 * Free SDL windows and renderer.
 */
void cleanup(void);

#endif /* INIT_H */
