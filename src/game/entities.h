/**
* \file entities.h
 *
 * Header file for managing entities.
 */

#ifndef ENTITIES_H
#define ENTITIES_H
#include "../structs.h"

/**
 * Initialize the entities. Set the textures and the properties of each.
 */
void initEntities(void);

/**
 * Draw the list of entities.
 */
void drawEntities(void);

/**
 * Interact with the list of entities.
 */
void doEntities(void);

/**
 * Remove all dead entities from the list.
 */
void cleanEntities(void);

/**
 * Create a new entity by duplicating his template.
 * Set his color and position.
 */
Entity *createEntity(int x, int y,
        enum entity_type type, enum color team, int life);

/**
 * Move an entity [e] at [x] and [y] coordinates if possible.
 */
void moveEntity(Entity *e, int x, int y);

/**
 * Get the first entity in the list of entities at coordinate [x],[y].
 * If [last] is NULL, then search start from the beginning of the list.
 * Else the search begin at the entity after [last] in the list.
 *
 * \return An entity or NULL if none have been found.
 */
Entity *findByPos(int x, int y, Entity *last);

/**
 * Find an entity by its id.
 */
Entity *findById(int id);

/**
 * Save information of entity [e] into entity info [ei].
 */
void getEntityInfo(Entity *e, EntityInfo *ei);

/**
 * Create a new entity using the information in [ei].
 */
void useEntityInfo(EntityInfo *ei);

#endif /* ENTITIES_H */
