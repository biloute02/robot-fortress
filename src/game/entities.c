/**
 * \file entities.c
 */

#include <SDL2/SDL.h>
#include <stdarg.h>
#include "entities.h"
#include "fortress.h"
#include "map.h"
#include "borders.h"
#include "../main.h"
#include "../defs.h"
#include "../structs.h"
#include "../system/texture.h"
#include "../system/draw.h"

static Entity entities[NB_ENTITIES] = {0};

/**
 * Load the textures and initialize default values for different type of 
 * entities into the global variable [entities].
 */
static void loadEntities(void);

/**
 * Free the list of entitites from memory.
 */
static void freeEntities(void);

/**
 * Create an array of size NB_ACTIONS. [count] is the number of actions
 * to initialize in this array, followed by [count] action arguments
 * The array is allocated and must be freed.
 */
static int *createEntityActions(int num, ...);

void
initEntities(void)
{
    loadEntities();
    freeEntities();
}

void
drawEntities(void)
{
    Entity *e;
    int x, y, t;

    for (e = fortress.entityList; e != NULL; e = e->next) {
        if (e->type == MINE && e->team != fortress.team) {
            continue;
        }

        x = e->x * TILE_SIZE + fortress.renderOffset.x;
        y = e->y * TILE_SIZE + fortress.renderOffset.y;

        t = e->life - 1;
        if (e->team != NOCOLOR) {
            t += e->team * e->nb_textures;
        }

        blit(e->textures[t], x, y, SCALE);
    }
}

void
doEntities(void)
{
    Entity *e;
    SDL_Point p = {app.mouse.x, app.mouse.y};
    SDL_Rect r = {0};

    for (e = fortress.entityList; e != NULL; e = e->next) {
        if (app.mouse.button[SDL_BUTTON_LEFT]) {
            r.x = e->x * TILE_SIZE + fortress.renderOffset.x;
            r.y = e->y * TILE_SIZE + fortress.renderOffset.y;
            r.w = TILE_SIZE;
            r.h = TILE_SIZE;

            if (SDL_PointInRect(&p, &r)) {

                // If the entity is of the color of the player
                // and not the same as the previous selected.
                if (e->team == fortress.team
                        && fortress.selected != e) {
                    fortress.selected = e;
                    fortress.action = e->defaultAction;
                    fortress.targeted = NULL;
                    app.mouse.button[SDL_BUTTON_LEFT] = 0;

                // If an entity has already been selected
                // and has attack action
                // and the entity is ennemy ant not a mine
                // and within reach.
                } else if (fortress.selected != NULL
                        && fortress.selected->actions[ATTACK]
                        && e->team != fortress.team
                        && e->type != MINE
                        && distMap(fortress.selected->x,
                                   fortress.selected->y,
                                   e->x, e->y) == 1) {
                    fortress.targeted = e;
                    // Don't reset the button left.
                    // Action to do with the targeted.

                // If an entity has already been selected and is the same
                } else if (fortress.selected != NULL
                        && fortress.selected == e) {
                    resetSelection();
                    app.mouse.button[SDL_BUTTON_LEFT] = 0;
                }

                // Quit the for loop.
                break;
            }
        }
    }
}

void 
cleanEntities(void)
{
    Entity *e, *prev;
    e = fortress.entityList;
    prev = NULL;
    while (e != NULL) {
        if (e->life == 0) {
            if (e == fortress.entityList) {
                fortress.entityList = e->next;
                free(e);
                e = fortress.entityList;
            } else {
                if (e == fortress.entityTail) {
                    fortress.entityTail = prev;
                }
                prev->next = e->next;
                free(e);
                e = prev->next;
            }
        } else {
            prev = e;
            e = e->next;
        }
    }
}

Entity *
createEntity(int x, int y, enum entity_type type, enum color team, int  life)
{
    Entity *e;
    e = (Entity *) malloc(sizeof(Entity));
    memcpy(e, &entities[type], sizeof(Entity));

    fprintf(stderr, "create entity id: %d\n", fortress.id);
    e->id = fortress.id;
    e->x = x;
    e->y = y;
    e->turn = fortress.turn;
    e->life = life;

    e->type = type;
    e->team = team;

    if (fortress.entityList == NULL) {
        fortress.entityList = fortress.entityTail = e;
    } else {
        fortress.entityTail->next = e;
        fortress.entityTail = e;
    }

    fortress.id++;
    return e;
}

void
moveEntity(Entity *e, int x, int y)
{
    e->x = x; e->y = y;
}

Entity *
findByPos(int x, int y, Entity *last)
{
    Entity *e;
    if (last != NULL) {
        e = last->next;
    } else {
        e = fortress.entityList;
    }
    while (e != NULL) {
        if (e->x == x && e->y == y && e->life > 0) {
            break;
        }
        e = e->next;
    }
    return e;
}

Entity *
findById(int id)
{
    Entity *e;

    e = fortress.entityList;
    while (e != NULL) {
        if (e->id == id) {
            break;
        }
        e = e->next;
    }
    return e;
}

void
getEntityInfo(Entity *e, EntityInfo *ei)
{
    ei->id   = e->id;
    ei->type = e->type;
    ei->team = e->team;
    ei->x    = e->x;
    ei->y    = e->y;
    ei->life = e->life;
}

void
useEntityInfo(EntityInfo *ei)
{
    createEntity(ei->x, ei->y, ei->type, ei->team, ei->life);
}

//updateEntities(EntityInfo *

static void
loadEntities(void)
{
    // If the entities have been loaded, do nothing.
    static int loaded = 0;
    if (loaded) return;

    // Robot
    entities[ROBOT].textures = loadAllTextures("gfx/robot", NB_ROBOT, 1);
    entities[ROBOT].nb_textures = NB_ROBOT;
    entities[ROBOT].actions = createEntityActions(3, MOVE, ATTACK, BUILD);
    entities[ROBOT].defaultAction = MOVE;

    // Fortress
    entities[FORTRESS].textures = loadAllTextures("gfx/fortress", NB_FORTRESS, 1);
    entities[FORTRESS].nb_textures = NB_FORTRESS;
    entities[FORTRESS].actions = createEntityActions(3, MOVE, DEPLOY, UPGRADE);
    entities[FORTRESS].defaultAction = DEPLOY;

    // Bomb
    entities[MINE].textures = loadAllTextures("gfx/mine", NB_MINE, 1);
    entities[MINE].nb_textures = NB_MINE;
    entities[MINE].actions = createEntityActions(1, NOACTION);

    // Watchtower
    entities[WATCHTOWER].textures = loadAllTextures("gfx/watchtower", NB_WATCHTOWER, 1);
    entities[WATCHTOWER].nb_textures = NB_WATCHTOWER;
    entities[WATCHTOWER].actions = createEntityActions(1, NOACTION);

    // Bridge
    entities[BRIDGE].textures = loadAllTextures("gfx/bridge", NB_BRIDGE, 0);
    entities[BRIDGE].nb_textures = NB_BRIDGE;
    entities[BRIDGE].actions = createEntityActions(1, NOACTION);

    loaded = 1;
}

static void 
freeEntities(void)
{
    Entity *e;
    while (fortress.entityList != NULL) {
        e = fortress.entityList->next;
        free(fortress.entityList);
        fortress.entityList = e;
    }
    fortress.entityList = fortress.entityTail = NULL;
}

static int *
createEntityActions(int count, ...)
{
    va_list ap;
    int i;
    int *actions;

    actions = (int *) calloc(NB_ACTIONS, sizeof(int));
    memset(actions, 0, NB_ACTIONS);

    va_start(ap, count);
    for (i = 0; i < count; i++) {
        actions[va_arg(ap, int)] = 1;       
    }
    va_end(ap);

    return actions;
}
