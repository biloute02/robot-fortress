#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "menu.h"
#include "widgets.h"
#include "game.h"
#include "fortress.h"
#include "../main.h"
#include "../defs.h"
#include "../system/network.h"

static void
draw(void)
{
    drawWidgets();
}

static void
logic(void)
{
    doWidgets();
}

void
initMenu(void)
{
    Widget *w;
    int width, height;
    char ip[30];

    initWidgets();

    w = createWidget("Robot Fortress", NULL);
    SDL_QueryTexture(w->texture, NULL, NULL, &width, NULL);
    w->pos.x = (SCREEN_WIDTH - width) / 2;
    w->pos.y = 10;
    
    w = createWidget("Partie Solo", singleplayerGame);
    w->pos.x = 10;
    w->pos.y = 100;
    
    w = createWidget("Partie Multijoueur", multiplayerGame);
    w->pos.x = 10;
    w->pos.y = 150;

    w = createWidget("Rejoindre une partie", joinGame);
    w->pos.x = 10;
    w->pos.y = 200;

    w = createWidget("Charger la dernière partie", loadGame);
    w->pos.x = 10;
    w->pos.y = 250;

    sprintf(ip, "Votre IP : %s", getIP());
    w = createWidget(ip, NULL);
    SDL_QueryTexture(w->texture, NULL, NULL, &width, &height);
    w->pos.x = (SCREEN_WIDTH - width) / 2;
    w->pos.y = SCREEN_HEIGHT - height - 10;

    app.delegate.draw = draw;   
    app.delegate.logic = logic;   
}

void
interruptMenu()
{
    app.delegate.interrupt = initMenu;
}
