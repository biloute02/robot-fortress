/**
 * \file actionLogic.h
 *
 * Header file for managing the action logic.
 */

#ifndef ACTIONLOGIC_H
#define ACTIONLOGIC_H
#include "../defs.h"
#include "../structs.h"

/**
 * If an entity is selected, do the action choosen for this unit.
 */
void doActionLogic(void);

/**
 * Create a new action info in the global array of actions happening this turn.
 */
void newActionInfo(void);

/**
 * Do all the actions in global variable actionInfo by calling doActionLogic for each.
 */
void useAllActionInfo(void);

#endif /* ACTIONLOGIC_H */
