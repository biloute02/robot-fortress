/**
 * \file actionMenu.h
 *
 * Header file for managing the action window.
 */

#ifndef ACTIONMENU_H
#define ACTIONMENU_H
#include "../defs.h"
#include "../structs.h"

/**
 * Initialize the action window.
 */
void initActionMenu(void);

/**
 * If an entity is selected, do the action choosen for this unit.
 */
void doActionMenu(void);

/**
 * Draw the action window.
 */
void drawActionMenu(void);

#endif /* ACTIONMENU_H */
