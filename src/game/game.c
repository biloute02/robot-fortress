#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "game.h"
#include "fortress.h"
#include "entities.h"
#include "../main.h"
#include "../structs.h"
#include "../system/network.h"

void
initGame(void)
{
}

void
singleplayerGame(void)
{
    fortress.mode = SINGLEPLAYER;
    fortress.role = SERVER;
    interruptFortress();
}

void
multiplayerGame(void)
{
    fortress.mode = MULTIPLAYER;
    fortress.role = SERVER;
    if (app.net.serverSockfd == 0) {
        initServer();
    }
    if (acceptClient() != 0) {
        return;
    }
    interruptFortress();
}

void
joinGame(void)
{
    fortress.mode = MULTIPLAYER;
    fortress.role = CLIENT;
    if (initClient() != 0) {
        return;
    }
    interruptFortress();
}


void 
loadGame(void)
{
}

void
saveGame(void)
{
    /*
    fopen("save/red.data"
    fopen("save/blue.data"
    fopen("save/.data"
    */
}
