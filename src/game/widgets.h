/**
 * \file widgets.h
 *
 * Header file for managing interactive areas on screen.
 */

#ifndef WIDGETS_H
#define WIDGETS_H

#include "../structs.h"

/**
 * Init the list of widgets.
 */
void initWidgets(void);

/**
 * Draw the list of widgets.
 */
void drawWidgets(void);

/**
 * Interact with the list of widgets.
 */
void doWidgets(void);

/**
 * Create a new widget.
 *
 * \param text NULL or the text to display.
 * \param action NULL or pointer to function to execute when clicking on it.
 */
Widget *createWidget(char *text, void (*action)(void));

#endif /* WIDGETS_H */
