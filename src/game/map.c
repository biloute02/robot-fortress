/**
 * \file map.c
 */

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include "map.h"
#include "fortress.h"
#include "../defs.h"
#include "../structs.h"
#include "../system/texture.h"
#include "../system/util.h"
#include "../system/draw.h"

Tile tiles[NB_TILES];

/**
 * Load the textures of the tiles from gfx directory into the global variable [tiles].
 */
static void loadTiles(void);

/**
 * Load a map from map directory into the global variable map.
 */
static void loadMap(void);

void
initMap(void)
{
    loadTiles();

    loadMap();
}

void
drawMap(void)
{
    int x, y, n, random;
    srand(fortress.map.seed);
    for (y = 0; y < MAP_HEIGHT; y++) {
        for (x = 0; x < MAP_WIDTH; x++) {
            n = fortress.map.data[y][x];
            if (0 <= n && n < NB_TILES) {
                // Faire varier les textures du tableau de texture.
                // TODO inefficient to calculate rand() at each render.
                random = rand() % tiles[n].nb_textures;
                blit(tiles[n].textures[random], (x * TILE_SIZE) + fortress.renderOffset.x,
                        (y * TILE_SIZE) + fortress.renderOffset.y, SCALE);
            }
        }
    }
}

int
distMap(int ax, int ay, int bx, int by) {
    return ABS(ax - bx) + ABS(ay - by);
}

int
insideMap(int x, int y) {
    return x >= 0 && y >= 0 && x < MAP_WIDTH && y < MAP_HEIGHT;
}

static void
loadTiles(void)
{
    // If the tiles have been loaded, do nothing.
    static int loaded = 0;
    if (loaded) return;

    // Grass
    tiles[GRASS].textures = loadAllTextures("gfx/grass", NB_GRASS, 0);
    tiles[GRASS].nb_textures = NB_GRASS; 

    // Mountain
    tiles[MOUNTAIN].textures = loadAllTextures("gfx/mountain", NB_MOUNTAIN, 0);
    tiles[MOUNTAIN].nb_textures = NB_MOUNTAIN;

    // Water
    tiles[WATER].textures = loadAllTextures("gfx/water", NB_WATER, 0);
    tiles[WATER].nb_textures = NB_WATER;

    loaded = 1;
}

static void
loadMap()
{
    if (fortress.map.num == 0) {
        fprintf(stderr, "loadMap: the map number is not initialised\n");
        exit(EXIT_FAILURE);
    }

    // If the map is already in memory, do nothing.
    static int num_loaded = 0;
    if (num_loaded == fortress.map.num) return;
    num_loaded = fortress.map.num;
    
    int x, y;
    char filename[MAX_FILENAME_LENGTH];
    char **tab_of_lines;
    char **p_tab;
    char *p_line;

    sprintf(filename, "map/%d.txt", fortress.map.num);
    tab_of_lines = readFile(filename);

    p_tab = tab_of_lines;
    for (y = 0; y < MAP_HEIGHT; y++) {

        p_line = *p_tab;
        for (x = 0; x < MAP_WIDTH; x++) {
            fortress.map.data[y][x] = atoi(p_line);
            do {p_line++;} while (*p_line != ' ' && *p_line != '\0');
        }
        free(*p_tab);

        p_tab++;
    }

    free(tab_of_lines);
}
