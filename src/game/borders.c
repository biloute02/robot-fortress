/**
 * \file borders.c
 */

#include <SDL2/SDL.h>
#include "borders.h"
#include "fortress.h"
#include "../defs.h"
#include "../structs.h"
#include "../system/texture.h"
#include "../system/draw.h"

static SDL_Texture *border_s;   /**< Texture to border the selected unit */
static SDL_Texture *border_t;   /**< Texture to border the targeted unit */
static SDL_Texture *border_a;   /**< Texture to border the selected action*/

void
initBorders(void)
{
    if (border_s == NULL)
        border_s = loadTexture("gfx/border/selected.png");
    if (border_t == NULL)
        border_t = loadTexture("gfx/border/targeted.png");
    if (border_a == NULL)
        border_a = border_t;
}

void
drawEntityBorders(void)
{
    int x, y;
    if (fortress.selected != NULL) {
        x = fortress.selected->x * TILE_SIZE + fortress.renderOffset.x;
        y = fortress.selected->y * TILE_SIZE + fortress.renderOffset.y;
        blit(border_s, x, y, SCALE);
    }

    if (fortress.targeted != NULL) {
        x = fortress.targeted->x * TILE_SIZE + fortress.renderOffset.x;
        y = fortress.targeted->y * TILE_SIZE + fortress.renderOffset.y;
        blit(border_t, x, y, SCALE);
    }
}

void
drawActionBorders()
{
    if (fortress.selected != NULL && fortress.action != NOACTION) {
        
    }
}
