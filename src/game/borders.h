/**
 * \file borders.h
 *
 * Header file for adding a border arround a texture.
 */

#ifndef BORDERS_H
#define BORDERS_H
#include "../structs.h"

/**
 * Initialize the textures.
 */
void initBorders(void);

/**
 * Draw the borders arround the selected and target units.
 */
void drawEntityBorders(void);

/**
 * Draw the border arround the selected action when a unit is selected.
 */
void drawActionBorders(void);

#endif /* BORDERS_H */
