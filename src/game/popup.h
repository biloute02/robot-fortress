/**
 * \file popup.h
 *
 * Header file for the menu functions.
 */

#ifndef POPUP_H
#define POPUP_H

/**
 * Initialize the popup.
 */
void initPopup(void);

/**
 * Load the popup.
 */
void interruptPopup();

#endif /* MENU_H */
