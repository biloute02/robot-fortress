/**
 * \file actionLogic.c
 */

#include <SDL2/SDL.h>
#include "actionMenu.h"
#include "entities.h"
#include "fortress.h"
#include "map.h"
#include "../main.h"
#include "../defs.h"
#include "../structs.h"
#include "../system/texture.h"
#include "../system/draw.h"

static ActionIcon actions[NB_ACTIONS];
static ActionIcon actionList;

/**
 * Load the textures of the actions from gfx/icons directory.
 */
static void loadActions(void);

/**
 * Free the list of actions.
 */
static void freeAction(void);

void
initActionMenu(void)
{
    loadActions();
}

void
doActionMenu(void)
{
    return;
}

void
drawActionMenu(void)
{
    SDL_Rect rect;
    SDL_Color bg = {0xf1, 0xf2, 0xbf, 255};
    int x, y, count;

    static Entity *e = NULL;
    if (e != fortress.selected) {

        e = fortress.selected;
    }
    
    if (e != NULL && !e->actions[NOACTION]) {

        x = (e->x) * TILE_SIZE + fortress.renderOffset.x;
        y = (e->y + 1) * TILE_SIZE + fortress.renderOffset.y - ICON_SIZE / 2;

        rect.x = x;
        rect.y = y;
        rect.h = ICON_SIZE;
        rect.w = ICON_SIZE;
        
        // Display the main action menu.
        if (fortress.action != BUILD) {
            if (e->actions[MOVE]) {
                blitRect(&rect, bg);
                blit(actions[MOVE].texture, x, y, SCALE / 2);
                rect.x += ICON_SIZE;
                x += ICON_SIZE;
            }
            if (e->actions[DEPLOY]) {
                blitRect(&rect, bg);
                blit(actions[DEPLOY].texture, x, y, SCALE / 2);
                rect.x += ICON_SIZE;
                x += ICON_SIZE;
            }
            if (e->actions[UPGRADE]) {
                blitRect(&rect, bg);
                blit(actions[UPGRADE].texture, x, y, SCALE / 2);
                rect.x += ICON_SIZE;
                x += ICON_SIZE;
            }
            if (e->actions[BUILD]) {
                blitRect(&rect, bg);
                blit(actions[BUILD].texture, x, y, SCALE / 2);
                rect.x += ICON_SIZE;
                x += ICON_SIZE;
            }
        // Display the building action menu.
        } else {

        }
    }
}

static void
loadActions(void)
{
    // If the action textures have been loaded, do nothing.
    static int loaded = 0;
    if (loaded) return;

    actions[MOVE].texture = loadTexture("gfx/action/move.png");
    actions[ATTACK].texture = loadTexture("gfx/action/move.png");
    actions[BUILD].texture = loadTexture("gfx/action/build.png");
    actions[UPGRADE].texture = loadTexture("gfx/action/upgrade.png");
    actions[DEPLOY].texture = loadTexture("gfx/action/deploy.png");

    loaded = 1;
}
