/**
 * \file actionLogic.c
 */

#include <SDL2/SDL.h>
#include "actionLogic.h"
#include "entities.h"
#include "fortress.h"
#include "map.h"
#include "../main.h"
#include "../defs.h"
#include "../structs.h"
#include "../system/texture.h"
#include "../system/draw.h"

/**
 * call doActionLogic with the values of global actionInfo at index [i].
 */
static void useActionInfo(int i);

/**
 * Apply move action on entity [e]
 * \return 2 if the entity touched a mine.
 *         1 if the entity has move, 0 overwise.
 */
static int moveAction(Entity *e);

/**
 * Attack action of entity [attacker] on entity [defender].
 * \return 3 if the defender is dead,
 *         2 if the attacker is dead,
 *         1 if the attacker attacks and nobody is dead, 0 overwise.
 */
static int attackAction(Entity *attacker, Entity *defender);

/**
 * Create a new robot entity from the entity [e].
 * \return 1 if a robot has been deployed, 0 overwise.
 */
static int deployAction(Entity *e);

/**
 * Replace the entity [e] with a new building.
 * \return 2 if the building has been placed
 *         0 overwise.
 */
static int buildAction(Entity *e);

void
doActionLogic(void)
{
    int valid;

    // If the button have been pressed
    // and a unit has been selected
    // and an action have been selected
    // and has not played this turn.
    if (app.mouse.button[SDL_BUTTON_LEFT]
            && fortress.selected != NULL
            && fortress.action != NOACTION
            && fortress.selected->turn < fortress.turn) {

        //fprintf(stderr, "action: %d\n", fortress.action);
        //fprintf(stderr, "e->x: %d e->y: %d\n", fortress.selected->x, fortress.selected->y);
        // If the player is moving on an enemy entity
        // and entity has ATTACK action.
        if (fortress.targeted != NULL
                && fortress.selected->actions[ATTACK]) {
            fortress.action = ATTACK;
        }

        valid = 0;
        switch (fortress.action) {
            case MOVE:
                valid = moveAction(fortress.selected);
                break;
            case ATTACK:
                valid = attackAction(fortress.selected, fortress.targeted);
                break;
            case DEPLOY:
                valid = deployAction(fortress.selected);
                break;
            case BUILD:
                valid = buildAction(fortress.selected);
                break;
            default:
                break;
        }

        // Test if an action has occured and reset the selection.
        if (valid > 0) {
            fortress.selected->turn = fortress.turn;
            // In multiplayer mode,
            // we record only actions we are doing (useActionInfo).
            if (fortress.mode == MULTIPLAYER
                    && fortress.selected->team == fortress.team) {
                newActionInfo();
            }
            resetSelection();
        } 
        if (valid > 1) {
            cleanEntities();
        }

        app.mouse.button[SDL_BUTTON_LEFT] = 0;
    }
}

void
newActionInfo()
{
    if (fortress.nb_actions >= MAX_ACTIONS) {
        fprintf(stderr, "newActionInfo: limit of actions is reached\n");
        exit(EXIT_FAILURE);
    }

    fortress.actionInfo[fortress.nb_actions].action = fortress.action;
    fortress.actionInfo[fortress.nb_actions].x = app.mouse.x;;
    fortress.actionInfo[fortress.nb_actions].y = app.mouse.y;;
    fortress.actionInfo[fortress.nb_actions].action = fortress.action;
    if (fortress.selected != NULL)
        fortress.actionInfo[fortress.nb_actions].selectedId = fortress.selected->id;
    if (fortress.targeted != NULL)
        fortress.actionInfo[fortress.nb_actions].targetedId = fortress.targeted->id;
    fortress.actionInfo[fortress.nb_actions].building = fortress.building;

    fortress.nb_actions++;
}

static void
useActionInfo(int i)
{
    if (i >= MAX_ACTIONS) {
        fprintf(stderr, "useActionInfo: wrong value of index %d\n", i);
        exit(EXIT_FAILURE);
    }

    app.mouse.x = fortress.actionInfo[i].x;
    app.mouse.y = fortress.actionInfo[i].y;
    fortress.action = fortress.actionInfo[i].action;
    if (fortress.actionInfo[i].selectedId != 0)
        fortress.selected = findById(fortress.actionInfo[i].selectedId);
    if (fortress.actionInfo[i].targetedId != 0)
        fortress.targeted = findById(fortress.actionInfo[i].targetedId);
    fortress.building = fortress.actionInfo[i].building;

    app.mouse.button[SDL_BUTTON_LEFT] = 1; // We simulate a click
    doActionLogic();
    //resetSelection();
    //not needed. If action is correct, it is called by doActionLogic.
}

void
useAllActionInfo()
{
    int i;
    for (i = 0; i < fortress.nb_actions; i++) {
        useActionInfo(i);
    }
    memset(fortress.actionInfo, 0, fortress.nb_actions * sizeof(ActionInfo));
    fortress.nb_actions = 0;
}

static int
moveAction(Entity *e)
{
    Entity *entityFind;
    int x, y;
    x = getXMap();
    y = getYMap(); 

    if (!insideMap(x, y)
        || fortress.map.data[y][x] == MOUNTAIN
        || fortress.map.data[y][x] == WATER
        || distMap(x, y, e->x, e->y) != 1) {
        return 0;
    }

    entityFind = findByPos(x, y, NULL);
    if (entityFind != NULL) {
        if (entityFind->type == MINE) {
            e->life = 0;
            entityFind->life = 0;
            //entityFind->turn = fortress.turn; // Always update the turn.
            return 2;
        } else {
            return 0;
        }
    }
    moveEntity(e, x, y);
    return 1;
}

static int
attackAction(Entity *attacker, Entity *defender)
{
    int dif;

    dif = attacker->life - defender->life;
    if (dif < 0) {
        attacker->life = 0;
    } else if (dif == 0) {
        attacker->life -= 1;
        defender->life -= 1;
    } else {
        attacker->life -= 1;
        defender->life -= dif;
        if (defender->life <= 0) {
            defender->life = 0;
            moveAction(attacker);
        }
    }
    fprintf(stderr, "life attacker: %d\n", attacker->life);
    fprintf(stderr, "life defender: %d\n", defender->life);
    if (attacker->life == 0) return 2;
    if (defender->life == 0) return 3;
    return 1;
}

static int
deployAction(Entity *e)
{
    Entity *findEntity;
    int x, y;
    x = getXMap();
    y = getYMap();

    // Check if the placement is valid.
    if (!insideMap(x, y)
        || fortress.map.data[y][x] == MOUNTAIN
        || fortress.map.data[y][x] == WATER
        || distMap(x, y, e->x, e->y) != 1) {
        return 0;
    }

    // Check if there is an entity.
    findEntity = findByPos(x, y, NULL);
    if (findEntity != NULL) {
        // If there is a mine, a unit is deployed on the mine
        // and both are killed.
        if (findEntity->type == MINE) {
            findEntity->life = 0;
            return 2;
        // If there are other entities, we can't deploy
        } else {
            return 0;
        }
    }

    // If there is a free place, we create a new unit.
    createEntity(x, y, ROBOT, e->team, e->life);
    return 1;
}

static int
buildAction(Entity *e)
{
    e->life = 0;
    createEntity(e->x, e->y, fortress.building, e->team, 1);
    return 2;
}
