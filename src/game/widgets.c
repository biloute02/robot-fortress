#include "widgets.h"
#include "../main.h"
#include "../structs.h"
#include "../system/draw.h"
#include "../system/text.h"

static Widget *widgetList;
static Widget *widgetTail;

static void freeWidgets(void);

void
initWidgets(void)
{
    freeWidgets();
}

void
drawWidgets(void)
{
    Widget *w;
    for (w = widgetList; w != NULL; w = w->next) {
        blit(w->texture, w->pos.x, w->pos.y, 1);
    }
}

void
doWidgets(void)
{
    Widget *w;
    SDL_Point p = {app.mouse.x, app.mouse.y};
    if (app.mouse.button[SDL_BUTTON_LEFT]) {
        for (w = widgetList; w != NULL; w = w->next) {
            if (w->action != NULL) {
                if (SDL_PointInRect(&p, &w->pos)) {
                    w->action();

                    // Quit the for loop.
                    app.mouse.button[SDL_BUTTON_LEFT] = 0;
                    break;
                }
            }
        }
    }
}

Widget *
createWidget(char *text, void (*action)(void)) 
{
    // Initialisation of memory.
    Widget *w = (Widget *) malloc(sizeof(Widget));
    memset(w, 0, sizeof(Widget));
    if (widgetList == NULL) {
        widgetList = widgetTail = w;
    } else {
        widgetTail->next = w;
        widgetTail = w;
    }

    // If there is text.
    if (text != NULL) {
        w->texture = getTextTexture(text);
        SDL_QueryTexture(w->texture, NULL, NULL, &w->pos.w, &w->pos.h);
    }

    // If there is an action.
    if (action != NULL) {
        w->action = action;
    }

    return w;
}

static void
freeWidgets(void)
{
    Widget *w;
    while (widgetList != NULL) {
        w = widgetList->next;
        free(widgetList);
        widgetList = w;
    }
    widgetList = widgetTail = NULL;
}
