/**
 * \file game.h
 *
 * Header file for managing the game : creating, saving, loading...
 */

#ifndef GAME_H
#define GAME_H
#include "../structs.h"

/**
 * Initialize the game.
 */
void initGame(void);

/**
 * Load the game screen at the next iteration of the global loop.
 */
void interruptGame(void);

/**
 * Create a new singleplayer game.
 */
void singleplayerGame(void);

/**
 * Create a new multiplayer game,
 * initialize the server and wait for a connection.
 */
void multiplayerGame(void);

/**
 * Load an existing game, initialize a server and wait for a connection.
 */
void loadGame(void);

/**
 * Try to join a remote game with a valid hostname.
 */
void joinGame(void);

#endif /* GAME_H */
