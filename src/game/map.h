/**
 * \file map.h
 *
 * Header file for map functions
 */

#ifndef MAP_H
#define MAP_H
#include "../structs.h"

#define getXMap() ((app.mouse.x - fortress.renderOffset.x) / TILE_SIZE);

#define getYMap() ((app.mouse.y - fortress.renderOffset.y) / TILE_SIZE);

extern Tile tiles[NB_TILES];

/**
 * Initialize a map.
 */
void initMap(void);

/**
 * Draw the map on the renderer.
 */
void drawMap(void);

/**
 * Get the Manhattan distance between a and b.
 */
int distMap(int ax, int ay, int bx, int by);

/**
 * Return 1 if the coordinates are valide, else 0.
 */
int insideMap(int x, int y);

#endif /* MAP_H */
