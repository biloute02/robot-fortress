/**
 * \file menu.h
 *
 * Header file for the menu functions.
 */

#ifndef MENU_H
#define MENU_H

/**
 * Initialize the menu. Set the logic and drawing functions.
 */
void initMenu(void);

/**
 * Load the menu screen at the next iteration of the global loop.
 */
void interruptMenu();

#endif /* MENU_H */
