/**
 * \file fortress.h
 *
 * Header file for the game (robot-fortress is the name of the game);
 */

#ifndef FORTRESS_H
#define FORTRESS_H
#include "../structs.h"

extern Fortress fortress;

/**
 * Initialize the fortress.
 */
void initFortress(void);

/**
 * Fortress actions.
 */
void doFortress(void);

/**
 * Load the fortress screen at the next iteration of the global loop.
 */
void interruptFortress(void);

/**
 * Pass the turn at the next iteration of the global loop. 
 */
void interruptTurn(void);

/**
 * Pass his turn.
 */
void nextTurn(void);

/**
 * Reset the values to 0 for selecting entities and action.
 */
void resetSelection();

/**
 * Save information of global vairable fortess into info structure [fi].
 */
void getFortressInfo(FortressInfo *fd);

/**
 * Update the values in global variable with the info struture [fi]. 
 */
void useFortressInfo(FortressInfo *fi);

#endif /* FORTRESS_H */
