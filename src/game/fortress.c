#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <time.h>
#include "fortress.h"
#include "menu.h"
#include "widgets.h"
#include "map.h"
#include "entities.h"
#include "borders.h"
#include "actionMenu.h"
#include "actionLogic.h"
#include "../main.h"
#include "../structs.h"
#include "../system/network.h"

Fortress fortress;

static void
draw(void)
{
    // Keep this order.
    drawMap();

    drawEntities();
    drawEntityBorders();

    drawActionMenu();   // in actionMenu.c
    drawActionBorders();// in border.c

    drawWidgets();
}

static void
logic(void)
{
    // Keep this order.
    doFortress();
    doWidgets();
    doActionMenu();
    doEntities();
    doActionLogic();
}

void
initFortress(void)
{
    FortressInfo fortInfo;
    Widget *w;
    Entity *e;
    EntityInfo e_info;
    int x, y, nb_bytes;
    
    /**
     * Game configureation
     */
    // Initialize the configuration when the player is the server.
    // SINGLEPLAYER or MULTIPLAYER.
    if (fortress.role == SERVER) {
        fortress.map.seed = (int) time(NULL);
        srand(fortress.map.seed);
        fortress.map.num = rand() % NB_MAPS + 1;
        printf("map num: %d\n", fortress.map.num);
        fortress.team = rand() % 2;
    }

    // If in multiplayer, send or receive the configuration of the game.
    if (fortress.mode == MULTIPLAYER) {
        if (fortress.role == SERVER) {    
            getFortressInfo(&fortInfo);
            sendNet(&fortInfo, sizeof(fortInfo));
            fortress.team = (fortress.team + 1) % NB_COLORS;
        } else {
            recvNet(&fortInfo, sizeof(fortInfo));
            useFortressInfo(&fortInfo);
        }
    }

    /**
     * Graphics.
     */
    initWidgets();
    initMap();
    initEntities();
    initActionMenu();
    initBorders();

    w = createWidget("Menu (echap)", interruptMenu);
    w->pos.x = 10;
    w->pos.y = 10;
    w = createWidget("Prochain tour (espace)", interruptTurn);
    w->pos.x = SCREEN_WIDTH / 2;
    w->pos.y = 10;

    fortress.id = 1;
    fortress.turn = 0;
    fortress.nb_actions = 0;
    fortress.renderOffset.x = (SCREEN_WIDTH - (MAP_WIDTH * TILE_SIZE)) / 2;
    fortress.renderOffset.y = (SCREEN_HEIGHT - (MAP_HEIGHT * TILE_SIZE)) / 2;
    resetSelection();

    /**
     * Initial entities.
     */
    if (fortress.mode == SINGLEPLAYER) {
        createEntity(2, 2, FORTRESS, RED, 1);
        createEntity(3, 2, FORTRESS, RED, 2);
        createEntity(4, 2, FORTRESS, RED, 3);
        createEntity(5, 2, FORTRESS, RED, 4);
        createEntity(6, 2, FORTRESS, RED, 5);
        createEntity(7, 2, MINE    , RED, 1);
        createEntity(8, 2, WATCHTOWER, RED, 1);
        createEntity(4, 4, WATCHTOWER, BLUE, 1);
        createEntity(5, 4, MINE    , BLUE, 1);
        createEntity(6, 4, FORTRESS, BLUE, 5);
        createEntity(7, 4, FORTRESS, BLUE, 4);
        createEntity(8, 4, FORTRESS, BLUE, 3);
        createEntity(9, 4, FORTRESS, BLUE, 2);
        createEntity(10,5, FORTRESS, BLUE, 1);

        nextTurn(); // Pass the first turn after the units were created.
        nextTurn(); // For the other team too.

    } else if (fortress.mode == MULTIPLAYER) {
        // Depending of the color, randomly initialize the fortress entity
        // to the left or to the right.
        do {
        x = rand() % MAP_WIDTH;
        y = rand() % MAP_HEIGHT;
        } while (fortress.map.data[y][x] != GRASS);

        // Client is playing the second.
        // TODO randomize who is playing first.
        // Client receives initial fortress position.
        if (fortress.role == CLIENT) {
            nb_bytes = recvNet(&e_info, sizeof(e_info));
            useEntityInfo(&e_info);
        }

        e = createEntity(x, y, FORTRESS, fortress.team, 1);
        getEntityInfo(e, &e_info);
        sendNet(&e_info, sizeof(e_info));

        // Server receives initial fortress position.
        if (fortress.role == SERVER) {
            nb_bytes = recvNet(&e_info, sizeof(e_info));
            useEntityInfo(&e_info);
        }
        fortress.turn++; 
        
        // Client is receiving the first turn of Server.
        if (fortress.role == CLIENT) {
            fortress.turn++;
            nb_bytes = recvNet(&fortress.actionInfo, MAX_ACTIONS * sizeof(ActionInfo));
            fortress.nb_actions = nb_bytes / sizeof(ActionInfo);
            fprintf(stderr, "recv %d actions\n", fortress.nb_actions);
            useAllActionInfo();
        }
    }

    app.delegate.draw = draw;   
    app.delegate.logic = logic;
}

void
doFortress(void)
{
    if (app.keyboard[SDLK_SPACE]) {
        interruptTurn();
        app.keyboard[SDLK_SPACE] = 0;
    }
    if (app.keyboard[SDLK_ESCAPE]) {
        interruptMenu();
        app.keyboard[SDLK_ESCAPE] = 0;
    }
}

void
interruptFortress(void)
{
    app.delegate.interrupt = initFortress;
}

void
interruptTurn(void)
{
    app.delegate.interrupt = nextTurn;
}

void
nextTurn(void)
{
    int nb_bytes;
    static int count = 0;

    // SINGLEPLAYER
    if (fortress.mode == SINGLEPLAYER) {
        fortress.team = (fortress.team + 1) % NB_COLORS;
        count = (count + 1) % NB_COLORS;
        // Each time count equals to 0, we pass the turn;
        if (count != 0) return;
    }

    fortress.turn++;

    // MULTIPLAYER
    if (fortress.mode == MULTIPLAYER) {
        fprintf(stderr, "send %d actions\n", fortress.nb_actions);
        sendNet(&fortress.actionInfo,
                fortress.nb_actions * sizeof(ActionInfo));

        nb_bytes = recvNet(&fortress.actionInfo, MAX_ACTIONS * sizeof(ActionInfo));
        fortress.nb_actions = nb_bytes / sizeof(ActionInfo);
        fprintf(stderr, "recv %d actions\n", fortress.nb_actions);
        useAllActionInfo();
    }

    SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION,
            SDL_LOG_PRIORITY_INFO, "New turn: %d", fortress.turn);
}

void
resetSelection(void)
{
    fortress.selected = NULL;
    fortress.targeted = NULL;
    fortress.action = NOACTION;
}

void
getFortressInfo(FortressInfo *fi)
{
    fi->map_num  = fortress.map.num;
    fi->map_seed = fortress.map.seed;
    fi->team     = fortress.team;
}

void
useFortressInfo(FortressInfo *fi)
{
    fortress.map.num = fi->map_num;
    fortress.map.seed = fi->map_seed;
    fortress.team = fi->team;
}
