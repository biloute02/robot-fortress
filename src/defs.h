/**
 * \file defs.h
 *
 * Constants for the game.
 */

#ifndef DEFS_H
#define DEFS_H

#define SCREEN_WIDTH    800
#define SCREEN_HEIGHT   600

#define MAX_KEYBOARD_KEYS 350
#define MAX_MOUSE_KEYS 5

#define MAX_FILENAME_LENGTH 50

/**
 * Network.
 */
#define PORT "3490"
#define BACKLOG 10      // Max length of the queue pending connections.
#define MAX_ACTIONS 100 // Max of actions we can send at once.

/**
 * Gamemode for the game.
 */
enum gamemode {
    SINGLEPLAYER,
    MULTIPLAYER
};

/**
 * When playing multiplayer, the program is either server or client.
 */
enum network_role {
    SERVER,
    CLIENT
};

/**
 * The team colors.
 */
#define NB_COLORS 2
enum color {
    BLUE,
    RED,
    NOCOLOR, /**< Must be the last because colors are indexes in arrays. */
};


#define MAP_HEIGHT 8
#define MAP_WIDTH 12

#define REAL_TILE_SIZE 16   // Size in pixel of the images.
#define SCALE 4        // Zoom the textures of the map by SCALE.
#define TILE_SIZE (REAL_TILE_SIZE * SCALE)  // Pixel size of the tile.
#define ICON_SIZE (TILE_SIZE / 2)           // Pixel size of an action icon.

#define FONT_SIZE 20 // Font size of the text.

#define NB_MAPS 3

/**
 * Types of tiles.
 */
#define NB_TILES 3 

#define NB_GRASS 4 // Number of grass images.
#define NB_MOUNTAIN 5
#define NB_WATER 3
enum tile_t {
    GRASS,
    MOUNTAIN,
    WATER
};


/**
 * Type of entitites
 */
#define NB_ENTITIES 5
#define NB_BUILDING 2

#define NB_ROBOT 5 // Number of robot images.
#define NB_FORTRESS 5
#define NB_MINE 1
#define NB_WATCHTOWER 1
#define NB_BRIDGE 2
enum entity_type {
    ROBOT,
    FORTRESS,
    MINE,
    WATCHTOWER,
    BRIDGE,
};

#define NB_ACTIONS 6
enum entity_action {
    NOACTION,   /**< The default, the entity cannot interact. */
    MOVE,       /**< A unit can move. */
    ATTACK,     /**< A unit can attack an other. */
    BUILD,      /**< A unit can construct a building. */
    DEPLOY,     /**< A unit can create new robots. */
    UPGRADE     /**< A unit can upgrade itself. */
};

#define ABS(a) ((a) < 0 ? -(a) : (a))

#endif /* DEFS_H */
