/**
 * \file structs.h
 *
 * Header file for the structures of the game.
 */

#ifndef STRUCTS_H
#define STRUCTS_H
#include <SDL2/SDL.h>
#include "defs.h"

/**
 * Structure to hold mouse data.
 */
typedef struct {
    int x;
    int y;
    int button[MAX_MOUSE_KEYS];
} Mouse;


/**
 * Delegate for handling the logic and draw functions.
 */
typedef struct {
    void (*draw)(void);
    void (*logic)(void);
    void (*interrupt)(void);
} Delegate;

/**
 * Hold information about the network.
 */
typedef struct {
    int serverSockfd;   /**< Listening socket server. */
    int clientSockfd;   /**< Connected socket for exchanging data. */
    char *hostname;     /**< The hostname of the server to connect. */
    char *myIp;         /**< The ip of the player. */
} Network;

/**
 * Hold information about the application : window, keys, network.
 */
typedef struct {
    SDL_Window   *window;
    SDL_Renderer *renderer;
    Mouse mouse;
    int keyboard[MAX_KEYBOARD_KEYS];
    Delegate delegate;
    Network net;
    FILE *save;     /**< Pointer for loading or saving data on disk. */
} App;

/**
 * Text or button to interact with the player.
 */
typedef struct widget {
    SDL_Texture *texture;
    SDL_Rect pos;
    void (*action)(void);
    struct widget *next;
} Widget;

/**
 * A tile of the map. Has a set of properties and a number of textures.
 */
typedef struct {
    SDL_Texture **textures;
    int nb_textures;
} Tile;

/**
 * The map is a set of tiles which are numbered from 1 to NB_TILES
 */
typedef struct {
    int num;    /**< The map number. */    
    int seed;
    int data[MAP_HEIGHT][MAP_WIDTH]; 
} Map;

/**
 * An entity is create on a tile of the map and has a set of properties.
 */
typedef struct entity {
    int id;     /**< Unique id of the entity. */
    int x;      /**< x position on the map. */
    int y;      /**< y position on the map. */
    int turn;   /**< The turn at which the entity have been active. */
    int life;   /**< 0 if the unit is dead or greater. */

    enum entity_type type;
    int *actions; /**< Array of 1 or 0 when the unit can do an action. */
    enum entity_action defaultAction; /**< action when selecting a unit. */

    enum color team;/**< A color or NOCOLOR if the entity has no team. */
    int nb_textures;        
    SDL_Texture **textures;
    struct entity *next;
} Entity;

/**
 * Data of an entity to be transported (saved, send, received).
 */
typedef struct {
    int id;
    int type;
    int team;
    int x, y;
    int life;
} EntityInfo;

/**
 * Describe an action.
 */
typedef struct {
    enum entity_action action;
    int x, y;
    int selectedId;
    int targetedId;
    enum entity_type building;
} ActionInfo;

/**
 * Button for selecting action on the action menu.
 */
typedef struct actionIcon {
    int x;
    int y;
    int active; /**< If the action is displayed on the screen */

    // Either of the two is used.
    enum entity_action type; /**< Type of the action. */
    enum entity_type entity; /**< Entity for the build action. */

    SDL_Texture *texture;
    struct action *next;
} ActionIcon;

/**
 * The fortress contains the map and the entities.
 */
typedef struct {
    int turn;               /**< Number of turn */
    SDL_Point renderOffset; /**< To center the map and entities */
    Map map;
    int id;                 /**< Number of entity created */
    Entity *entityList, *entityTail;
    Entity *selected;       /**< The unit currently selected */
    Entity *targeted;       /**< The target of the action */
    enum entity_action action;   /**< Action for the selected unit */
    enum entity_type building;   /**< The entity to build */
    int nb_actions;              /**< Number of actions this turn */
    ActionInfo actionInfo[MAX_ACTIONS];
    enum color team;
    enum gamemode mode;
    enum network_role role; /**< Either server or client. Default server */
} Fortress;

typedef struct {
    int map_num;
    int map_seed;
    int team;
} FortressInfo;

#endif /* STRUCTS_H */
