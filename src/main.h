/**
 * \file main.h
 * 
 * Global variables.
 */

#ifndef MAIN_H
#define MAIN_H

#include <SDL2/SDL_ttf.h>
#include "structs.h"

extern App app;

#endif /* MAIN_H */
